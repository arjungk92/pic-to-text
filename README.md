## Image to ASCII converter

#### This application is a single page webapp that allows a user to upload an image and receive the ascii version of the same.

####&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Demo](https://www.pic2text.ca/index.html)

* No backend is required to service any of the conversions.
* All conversions are handled by pure javascript.


* Original image.

![Diddley](images/diddly-non-transparent.jpg) 

* Converted image.

![DIddley Text](images/diddly-text.png)
let label = document.getElementById("image-label");
let canvas = document.createElement('canvas');
imageInput = document.getElementById("img");
let charList = "@%#*+=-:. ";
let charListLen = charList.length;
let reloadPage = function () {
    location.reload();
}

let loadFile = { handleEvent: function(event) {
        let output = document.createElement("img");
        output.id = "selected-image";
        output.classList.add("output-image");
        event.target.parentNode.insertBefore(output, event.target.nextSibling);
        // let output = document.getElementById('selected-image');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            label.style.display = 'none';
            let image = document.getElementById("selected-image");
            let width = 75;

            // Apparently characters do not have an aspect ration of 1:1. Go figure.
            let squeezeFactor = 0.55;

            let height =  Math.floor(width * (image.height / image.width) * squeezeFactor);
            canvas.getContext('2d').drawImage(image, 0, 0, width, height);
            URL.revokeObjectURL(output.src) // free memory

            // Convert image down to greyscale and compute the mean "brightness" along the way.
            let total = 0;
            let greyScaleImage = new Array(height);
            for (let i = 0; i < height; i++) {
                greyScaleImage[i] = new Array(width);
                for (let j = 0; j < width; j++) {
                    let pixelData = canvas.getContext('2d').getImageData(j, i, 1, 1).data;
                    greyScaleImage[i][j] = (0.3 * pixelData[0] + 0.59 * pixelData[1] + 0.11 * pixelData[2]) * (pixelData[3] / 255);
                    total += greyScaleImage[i][j];
                }
            }

            // Create a string where dark areas will be replaced by @ and lighter areas by <space>.
            let average = total / (width * height);
            let text = "";
            for (let i = 0; i < height; i++) {
                for (let j = 0; j < width; j++) {
                    // Simple algo.
                    if(greyScaleImage[i][j] < average) {
                        text += "@";
                    }
                    else {
                        text += " ";
                    }

                    //Complex algo.
                    // text += charList.charAt(Math.round((greyScaleImage[i][j])))
                }
                text += "\n";
            }

            let textOutput = document.createElement('textarea');
            output.parentNode.insertBefore(textOutput, output.nextSibling);
            textOutput.value = text;
            textOutput.rows = height;
            textOutput.cols = width;

            let reloadButton = document.createElement("button");
            reloadButton.classList.add("label-image");
            reloadButton.textContent = "Select Another Image"
            reloadButton.addEventListener("click", reloadPage);
            textOutput.parentNode.insertBefore(reloadButton, textOutput.nextSibling);
        }
    }}
imageInput.addEventListener("change", loadFile);

var acc = document.getElementsByClassName("qn");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "flex") {
            panel.style.display = "none";
        } else {
            panel.style.display = "flex";
        }
    });
}